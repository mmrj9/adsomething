import {Mongo} from 'meteor/mongo';

export const WebContent = new Mongo.Collection('webcontent');

Meteor.startup(function() {
    if (Meteor.isServer) {
        Meteor.methods({
            'createWebContent': function(webcontent) {
                return WebContent.insert(webcontent);
            }
        });
    }
});
