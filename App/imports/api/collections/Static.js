import {Mongo} from 'meteor/mongo';

export const Static = new Mongo.Collection('static');

Meteor.startup(function() {
    if (Meteor.isServer) {
        //Categories
        const categories = [
            'Adult',
            'Arts and Entertainment',
            'Autos and Vehicles',
            'Beauty and Fitness',
            'Books and Literature',
            'Business and Industry',
            'Career and Education',
            'Computer and Electronics',
            'Finance',
            'Food and Drink',
            'Gambling',
            'Games',
            'Health',
            'Home and Garden',
            'Internet and Telecom',
            'Law and Government',
            'News and Media',
            'People and Society',
            'Pets and Animals',
            'Recreation and Hobbies',
            'Reference',
            'Science',
            'Shopping',
            'Sports',
            'Travel',
        ];

        const ageRanges = ['-18', '18 - 24', '25 - 44', '45 - 64', '+65']

        let objCategories = {
            name: 'categories',
            list: categories
        }

        let objAgeRanges = {
            name: 'ageRanges',
            list: ageRanges,
        }

        //Clear Collection
        Static.remove({});

        //Insert in Collection
        Static.insert(objCategories);
        console.log(`Created ${categories.length} default categories`);

        Static.insert(objAgeRanges);
        console.log(`Created ${ageRanges.length} default age ranges`);

        Meteor.methods({
            'getStaticList': function(name) {
                let obj = Static.findOne({name});
                return obj.list;
            }
        });
    }
})
