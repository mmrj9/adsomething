import React from 'react';

export default class Preview extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        var res;
        if (this.props.files && this.props.files.length > 0) {
            res = (
                <div>
                    {this.props.files.map(function(file, i) {
                        return <div style={styles.divPreviewStyle} key={i}>
                            <img style={styles.imagePreviewStyle} src={file.preview}/>
                        </div>
                    }.bind(this))}
                </div>
            )
        } else {
            res = (
                <div style={styles.placeholderText}>
                    Drop your assets here
                </div>
            )
        }
        return res;
    }
};

const styles = {
    dropzoneStyle: {
        "width": "100%",
        "borderWidth": "2px",
        "borderColor": "rgb(102, 102, 102)",
        "borderStyle": "dashed",
        "borderRadius": "5px",
        "minHeight": "100px"
    },
    placeholderText: {
        textAlign: 'center',
        marginTop: 25,
    },
    imagePreviewStyle: {
        "maxWidth": '200px',
        "height": 'auto',
        "marginLeft": '10px',
        "marginTop": '10px',
        "marginBottom": '10px'
    },
    divPreviewStyle: {
        "width": '200px',
        "height": '100px',
        "display": 'inline',
        "position": "relative"
    },
}
