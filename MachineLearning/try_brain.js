import mimir from 'mimir';
import brain from 'brain.js';
import {adultText} from '../Crawlers/extracted-data/Adult';
import {beautyText} from '../Crawlers/extracted-data/BeautyAndFitness';

/**
 * Utility functions
 */

function vec_result(res, num_classes) {
  var i = 0,
    vec = [];
  for (i; i < num_classes; i += 1) {
    vec.push(0);
  }
  vec[res] = 1;
  return vec;
}
function maxarg(array) {
  return array.indexOf(Math.max.apply(Math, array));
}

let adultData = adultText.split(',');
let beautyData = beautyText.split(',');
console.log(adultData.length);
console.log(beautyData.length);


let categories = {
  Adult: 0,
  BeautyAndFitness: 1,
};

let categoriesArray = Object.keys(categories);

let allData = adultData.concat(beautyData);
let dict = mimir.dict(allData);

let trainData = [];

console.log("Building train data...");
adultData.forEach((t) => {
  console.log([
    mimir.bow(t, dict),
    categories.Adult
  ]);
  trainData.push([
    mimir.bow(t, dict),
    categories.Adult
  ]);
})

beautyData.forEach((t) => {
  trainData.push([
    mimir.bow(t, dict),
    categories.BeautyAndFitness
  ]);
})

console.log(trainData.length);

let net = new brain.NeuralNetwork();
let categories_train = trainData.map(function(pair) {
  return {
    input: pair[0],
    output: vec_result(pair[1], categoriesArray.length)
  };
});

console.log("Trainning...");
net.train(categories_train, {
  errorThresh: 0.005, // error threshold to reach
  iterations: 2000, // maximum training iterations
  log: true, // console.log() progress periodically
  logPeriod: 1, // number of iterations between logging
});
console.log("Trained with success");

let test = "XXX movies";
console.log(`Predict: ${test}`);
let predict = net.run(mimir.bow(test,dict));
console.log(predict);
console.log(categoriesArray[maxarg(predict)]);
