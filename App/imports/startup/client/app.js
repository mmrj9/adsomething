import { Meteor } from 'meteor/meteor';

import React from 'react';
import ReactDOM from 'react-dom';

import MainLayout from '../../ui/containers/MainLayout.js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import helpers from '../../helpers/helpers';

Meteor.startup(() => {
  ReactDOM.render(
  	  	<MuiThemeProvider>
    		<MainLayout />
  		</MuiThemeProvider>
    	,
    	document.getElementById('app')
  	);
});
