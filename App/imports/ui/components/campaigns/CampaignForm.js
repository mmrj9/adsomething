//Package Imports
import React from 'react';
import {Step, Stepper, StepLabel} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import TimePicker from 'material-ui/TimePicker';
import DatePicker from 'material-ui/DatePicker';
import Snackbar from 'material-ui/Snackbar';
import Toggle from 'material-ui/Toggle';
import Chip from 'material-ui/Chip';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import AutoComplete from 'material-ui/AutoComplete';
import Checkbox from 'material-ui/Checkbox';
import Slider from 'material-ui/Slider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Dropzone from 'react-dropzone';
import CountrySelect from "react-country-select";
import {_} from 'meteor/underscore';
//Local Imports
import AssetPreview from './AssetPreview';

injectTapEventPlugin();

//Image Max Size (5MB)
const FIVE_MiB = 5242880;

//Categories list
let categories;
let ageRanges;

export default class CampaignForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            //Stepper Data
            finished: false,
            stepIndex: 0,
            //Snackbar Data
            snackbarOpen: false,
            snackbarText: '',
            //Campaign Fields
            name: '',
            url: '',
            description: '',
            beginDate: null,
            endDate: null,
            beginTime: null,
            endTime: null,
            budget: 0,
            clicks: 0,
            clickValue: 0.1,
            assets: [],
            categories: [],
            keywords: [],
            countries: [],
            displayAdult: false,
            ageRange: '',
            publisherMinScore: 50,
            approvalRequired: true,
            //Date Toggles
            startNow: true,
            endBudget: true
        };
        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
        this.onDrop = this.onDrop.bind(this);
    }

    componentWillMount() {
        Meteor.call('getStaticList', 'categories', function(err, res) {
            categories = res;
        })
        Meteor.call('getStaticList', 'ageRanges', function(err, res) {
            ageRanges = res;
        })
    }

    componentDidUpdate() {}

    // Stepper - Handle Next Step
    handleNext() {
        const {stepIndex, finished} = this.state;
        //If not last step, go to next step
        if (stepIndex < 2) {
            this.setState({
                stepIndex: stepIndex + 1
            }
            //If last step - create campaing
            )
        } else {
            //Get only campaing fields from state
            const {
                name,
                url,
                description,
                startNow,
                endBudget,
                beginDate,
                endDate,
                beginTime,
                endTime,
                clickValue,
                budget,
                clicks,
                categories,
                keywords,
                countries,
                displayAdult,
                ageRange,
                publisherMinScore,
                approvalRequired
            } = this.state;
            //Create campaing object
            let campaing = {
                userId: Meteor.userId(),
                name,
                url,
                description,
                beginDate: (startNow
                    ? new Date()
                    : beginDate),
                endDate: (endBudget
                    ? addYears(new Date(), 1)
                    : endDate),
                beginTime: (startNow
                    ? new Date().getTime()
                    : beginTime),
                endTime: (endBudget
                    ? null
                    : endTime),
                clickValue,
                budget,
                clicks,
                categories,
                keywords,
                countries,
                displayAdult,
                ageRange,
                publisherMinScore,
                approvalRequired
            }
            //Call to Create Campaing
            Meteor.call("createCampaing", campaing, function(error, result) {
                if (error) {
                    console.log("error", error);
                }
                if (result) {
                    this.setState({finished: true, snackbarText: `Campaing ${campaing.name} successfully created`, snackbarOpen: true});
                }
            }.bind(this));
        }
    };

    // Stepper - Previous Step
    handlePrev() {
        const {stepIndex} = this.state;
        if (stepIndex > 0) {
            this.setState({
                stepIndex: stepIndex - 1
            });
        }
    };

    /*
    * TODO - Filter files (Size/MaxAssets/Repetitions/...)
    * Handles drop event on the image dropzone
    */
    onDrop(acceptedFiles, rejectedFiles) {
        console.log('Accepted files: ', acceptedFiles);
        console.log('Rejected files: ', rejectedFiles);
        let assets = this.state.assets.concat(acceptedFiles);
        this.setState({assets});
    }

    /*
     * TODO - Input Mask
     * Handles click value change
     */
    clickValueChange(event) {
        let clickValue;

        if (event.target.value == '0')
            clickValue = 0;
        else
            clickValue = parseFloat(event.target.value) || '';

        let budget = this.state.budget;
        // Clicks = Budget/ClickValue Ex: (100/0.12 = 833 cliks)
        let clicks = Math.floor(budget / clickValue);
        this.setState({budget: budget, clicks: clicks, clickValue: clickValue});
    }

    // Handles budget change
    budgetChange(event) {
        let clickValue = this.state.clickValue;
        let budget = parseFloat(currencyToValue(event.target.value)) || 0;
        // Clicks = Budget/ClickValue Ex: (100/0.12 = 833 cliks)
        let clicks = Math.floor(budget / clickValue);
        this.setState({budget: budget, clicks: clicks, clickValue: clickValue});
    }

    // Handles click amount change
    clickAmountChange(event) {
        let clicks = parseFloat(event.target.value) || 0;
        let clickValue = this.state.clickValue;
        // Budget = Clicks*ClickValue Ex: (833*12 = 100)
        let budget = clicks * clickValue;
        this.setState({budget: budget, clicks: clicks, clickValue: clickValue});
    }

    //Change Date Setting
    changeDateSetting(isStart) {
        //If change start campaing setting
        if (isStart)
            this.setState({
                startNow: !this.state.startNow
            })
            //Else change end campaing setting
        else
            this.setState({
                endBudget: !this.state.endBudget
            })
    }

    //Returns categoryitems for the select list
    categoryItems(values) {
        return categories.map((category, index) => (<MenuItem
            key={index}
            insetChildren={true}
            checked={values && values.includes(category)}
            value={category}
            primaryText={category}/>));
    }

    //Returns items for the select list
    ageRangeItems(values) {
        return ageRanges.map((range, index) => (<MenuItem key={index} value={range} primaryText={range}/>));
    }

    //Returns the currently selected keywords
    getKeywords() {
        return this.state.keywords.map((keyword, index) => (
            <Chip key={index} onRequestDelete={(event, index) => this.handleRequestDelete(keyword)}>
                {keyword}
            </Chip>

        ))
    }
    //Deletes chip
    handleRequestDelete(keyword) {
        let keywords = _.without(this.state.keywords, keyword);
        this.setState({keywords})
    }

    addKeyword(value) {
        this.refs.AutoComplete.setState({searchText: ''});
        if (_.contains(this.state.keywords, value))
            alert(`${value} is already a keyword`)
        else {
            this.state.keywords.push(value);
            this.setState({keywords: this.state.keywords});
        }
    }

    //Gets the content for each of the 3 steps
    getStepContent(stepIndex) {
        switch (stepIndex) {
            case 0:
                return (
                    <div>
                        <TextField
                            floatingLabelText="Name"
                            value={this.state.name}
                            onChange={(event) => this.setState({name: event.target.value})}/>
                        <TextField
                            floatingLabelText="Url"
                            value={this.state.url}
                            onChange={(event) => this.setState({url: event.target.value})}/>
                        <br/>
                        <TextField
                            hintText="Message Field"
                            floatingLabelText="Description"
                            value={this.state.description}
                            multiLine={true}
                            rows={2}
                            onChange={(event) => this.setState({description: event.target.value})}/>
                        <br/>
                        <Toggle
                            label={this.state.startNow
                            ? "Start as soon as possible"
                            : "Start at this date"}
                            defaultToggled={true}
                            toggled={this.state.startNow}
                            onToggle={() => this.changeDateSetting(true)}/> {this.state.startNow
                            ? <div></div>
                            : <div>
                                <DatePicker
                                    hintText="Begin Date"
                                    minDate={new Date()}
                                    maxDate={addDays(addYears(new Date, 1), -7)}
                                    value={this.state.beginDate}
                                    onChange={(event, date) => this.setState({beginDate: date})}/>
                                <TimePicker
                                    format="24hr"
                                    hintText="Begin Time"
                                    value={this.state.endTime}
                                    onChange={(event, time) => this.setState({beginTime: time})}/></div>}
                        <br/>
                        <Toggle
                            label={this.state.endBudget
                            ? "Finish when budget ends"
                            : "Finish at this date"}
                            defaultToggled={true}
                            toggled={this.state.endBudget}
                            onToggle={() => this.changeDateSetting(false)}/> {this.state.endBudget
                            ? <div></div>
                            : <div>
                                <DatePicker
                                    hintText="End Date"
                                    defaultDate={this.state.beginDate
                                    ? addDays(this.state.beginDate, 7)
                                    : addDays(new Date(), 7)}
                                    minDate={this.state.beginDate
                                    ? addDays(this.state.beginDate, 7)
                                    : addDays(new Date(), 7)}
                                    maxDate={addYears(new Date, 1)}
                                    value={this.state.endDate}
                                    onChange={(event, date) => this.setState({endDate: date})}/>
                                <TimePicker
                                    format="24hr"
                                    hintText="End Time"
                                    value={this.state.endTime}
                                    onChange={(event, time) => this.setState({endTime: time})}/></div>}
                        <br/>
                        <TextField
                            floatingLabelText="Click Value"
                            value={valueToCurrency(toDecimal(this.state.clickValue))}
                            onChange={(event) => this.clickValueChange(event)}/>
                        <br/>
                        <TextField
                            floatingLabelText="Budget"
                            value={valueToCurrency(this.state.budget)}
                            onChange={(event) => this.budgetChange(event)}/>
                        <TextField floatingLabelText="Clicks" value={this.state.clicks} onChange={(event) => this.clickAmountChange(event)}/>
                        <div style={{
                            textAlign: 'right'
                        }}>
                            <a onClick={() => {
                                this.setState({assets: null})
                            }}>Clear files</a>
                        </div>
                        <Dropzone style={styles.dropzoneStyle} onDrop={this.onDrop} accept="image/*">
                            <AssetPreview files={this.state.assets}/>
                        </Dropzone>
                    </div>
                );
            case 1:
                return (
                    <div>
                        <SelectField
                            multiple={true}
                            hintText="Select categories (3 Max)"
                            value={this.state.categories}
                            onChange={(event, index, values) => {
                            if (values.length <= 3)
                                this.setState({categories: values})
                        }}>
                            {this.categoryItems(this.state.categories)}
                        </SelectField>
                        <br/>
                        <AutoComplete
                            ref='AutoComplete'
                            floatingLabelText="Type some keywords"
                            filter={AutoComplete.fuzzyFilter}
                            dataSource={this.state.keywords}
                            maxSearchResults={5}
                            onNewRequest={(value, index) => this.addKeyword(value)}/>
                        <div
                            style={{
                            display: 'flex',
                            flexWrap: 'wrap'
                        }}>
                            {this.getKeywords()}
                        </div>
                        <br/>
                        <CountrySelect multi={true} flagImagePath="/img/flags/" onSelect={(countries) => this.setState({countries})}/>
                        <br/>
                        <Checkbox
                            label="Display on Adult Sites"
                            checked={this.state.displayAdult}
                            onCheck={(event, displayAdult) => this.setState({displayAdult})}/>
                        <SelectField
                            floatingLabelText="Specify Age Range"
                            value={this.state.ageRange}
                            onChange={(event, index, ageRange) => this.setState({ageRange})}>
                            {this.ageRangeItems()}
                        </SelectField>
                    </div>
                );
            case 2:
                return (
                    <div>
                        <div style={{
                            textAlign: 'center'
                        }}>
                            <h3>{this.state.publisherMinScore}</h3>
                            <span>Publisher Minimum Score</span>
                        </div>
                        <Slider
                            defaultValue={this.state.publisherMinScore}
                            value={this.state.publisherMinScore}
                            onChange={(event, publisherMinScore) => this.setState({publisherMinScore})}
                            min={0}
                            max={100}
                            step={1}/>
                        <Checkbox
                            label="Is approval required?"
                            checked={this.state.approvalRequired}
                            onCheck={(event, approvalRequired) => this.setState({approvalRequired})}/>
                    </div>
                );
            default:
                return 'Ops you shouldn\'t be here ';
        }
    }

    render() {
        const contentStyle = {
            margin: '0 16px'
        };

        return (
            <div
                style={{
                width: '100%',
                maxWidth: 700,
                margin: 'auto'
            }}>
                <h1>Campaign Form</h1>
                <Stepper activeStep={this.state.stepIndex}>
                    <Step>
                        <StepLabel>General Data</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Traffic</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>???</StepLabel>
                    </Step>
                </Stepper>
                <div style={contentStyle}>
                    {this.state.finished
                        ? (
                            <p>
                                <a
                                    href="#"
                                    onClick={(event) => {
                                    event.preventDefault();
                                    this.setState({stepIndex: 0, finished: false});
                                }}>
                                    Click here
                                </a>
                                to reset the example.
                            </p>
                        )
                        : (
                            <div>
                                {this.getStepContent(this.state.stepIndex)}
                                <div style={{
                                    marginTop: 12
                                }}>
                                    <FlatButton
                                        label="Back"
                                        disabled={this.state.stepIndex === 0}
                                        onTouchTap={this.handlePrev}
                                        style={{
                                        marginRight: 12
                                    }}/>
                                    <RaisedButton
                                        label={this.state.stepIndex === 2
                                        ? 'Finish'
                                        : 'Next'}
                                        primary={true}
                                        onTouchTap={this.handleNext}/>
                                </div>
                            </div>
                        )
}
                </div>
                <Snackbar
                    open={this.state.snackbarOpen}
                    message={this.state.snackbarText}
                    autoHideDuration={4000}
                    onRequestClose={() => this.setState({snackbarText: '', snackbarOpen: false})}/>
            </div>
        );
    }
}
const styles = {
    dropzoneStyle: {
        "width": "100%",
        "borderWidth": "2px",
        "borderColor": "rgb(102, 102, 102)",
        "borderStyle": "dotted",
        "borderRadius": "5px",
        "minHeight": "100px"
    }
}
