export const data = [
  {
    "category": "Science",
    "urls": "researchgate.net infourok.ru noaa.gov rae.es mathworks.com livescience.com howstuffworks.com scientificamerican.com futu" +
      "ra-sciences.com netdna-cdn.com sciencealert.com acs.org symbolab.com netdna-ssl.com wolfram.com brainly.com convertunits" +
      ".com rsc.org phys.org popmech.ru cyberleninka.ru newscientist.com minhaji.net webofknowledge.com calculatorsoup.com inve" +
      "rse.com cnpq.br technet.idnes.cz jst.go.jp pandia.ru redalyc.org socratic.org revues.org isky.am cnrs.fr sciencesetaveni" +
      "r.fr aps.org wanfangdata.com.cn pnas.org iop.org mnn.com physicsclassroom.com violympic.vn flamp.ru r-bloggers.com cell." +
      "com sydsvenskan.se spektrum.de mpg.de mdpi.com ptable.com youmath.it ssrn.com dorar-aliraq.net hwaml.com geology.com web" +
      "qc.org cern.ch persee.fr gmo-mars.jp easycalculation.com disfrutalasmatematicas.com mathforum.org endmemo.com portall.zp" +
      ".ua graaam.com esa.int interestingengineering.com statisticshowto.com electronics-tutorials.ws 0calc.com inpe.br sobiolo" +
      "gia.com.br chemguide.co.uk explorable.com worldometers.info asm.org integral-calculator.com sciencebuddies.org scirp.org" +
      " klp.pl consoglobe.com rjeem.com thatquiz.org reprap.org tiger-algebra.com matematyka.pl cpm.org ewg.org friatider.se mo" +
      "ngabay.com savebt.com derivative-calculator.net unitjuggler.com matematiktutkusu.com degruyter.com satom.ru cepal.org jb" +
      "c.org corourbano.com "
  }
]
