export const data = [
  {
    "category": "Health",
    "urls": "nih.gov webmd.com drugs.com healthline.com mayoclinic.org medlineplus.gov nhs.uk walgreens.com health.mail.ru cvs.com fi" +
      "tbit.com psychologytoday.com doctissimo.fr medscape.com cdc.gov medicinenet.com abczdrowie.pl elsevier.com lifescript.co" +
      "m poradnikzdrowie.pl 120ask.com mercola.com baiducontent.com iherb.com jiankang.com everydayhealth.com healthgrades.com " +
      "carenoid.com patient.info who.int minhavida.com.br tuasaude.com xywy.com authoritynutrition.com whattoexpect.com draxe.c" +
      "om kaiserpermanente.org medhelp.org kidshealth.org baby.ru health.com womanadvice.ru womenshealthmag.com healthy.kaiserp" +
      "ermanente.org bethesda.net xaid.jp 39.net realself.com passeportsante.net feide.no biomedcentral.com care.com rxlist.com" +
      " guiainfantil.com hastanerandevu.gov.tr athenahealth.com psychcentral.com alodokter.com leafly.com apteka.ru mp.pl stead" +
      "yhealth.com tiensmed.ru netdoktor.de tunwalai.com doz.pl uptodate.com mdsaude.com medisite.fr ayzdorov.ru mindbodygreen." +
      "com piluli.ru antenam.biz medside.ru haodf.com rlsnet.ru cancer.org emedicinehealth.com fithacker.ru mundoboaforma.com.b" +
      "r healthcare.gov docomo-de.net activebeat.co hazipatika.com zocdoc.com myprotein.com altibbi.com superdrug.com cleveland" +
      "clinic.org mhrs.gov.tr piluli.kharkov.ua santeplusmag.com onmeda.de spine-health.com salud180.com vndoc.com doctolib.fr " +
      "vitacost.com myherbalife.com heart.org "
  }
]
