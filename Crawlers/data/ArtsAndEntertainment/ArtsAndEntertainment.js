export const data = [
  {
    "category": "Arts and Entertainment",
    "urls": "youtube.com netflix.com imdb.com wikia.com nicovideo.jp soundcloud.com dailymotion.com youtube-mp3.org xfinity.com 9gag." +
        "com spotify.com vimeo.com deviantart.com pixiv.net youku.com douban.com giphy.com pandora.com gfycat.com flickr.com go.c" +
        "om azlyrics.com kinopoisk.ru hulu.com eksisozluk.com douyu.com seasonvar.ru tubidy.mobi convert2mp3.net pikabu.ru fanfic" +
        "tion.net youtu.be genius.com shutterstock.com hotstar.com cda.pl mp3juices.cc rottentomatoes.com ultimate-guitar.com mus" +
        "ica.com my-hit.org behance.net subscene.com umblr.com myanimelist.net mangafox.me allocine.fr filmweb.pl vagalume.com.br" +
        " syosetu.com freepik.com deezer.com patreon.com programme-tv.net mediaset.it sky.com yaplakal.com acfun.cn primewire.ag " +
        "fishki.net animeflv.net music.yandex.ru diply.com mp3.zing.vn fmovies.to discogs.com gogoanime.io worldstarhiphop.com ka" +
        "pook.com tmz.com deviantart.net nhk.or.jp movie.douban.com crunchyroll.com ivi.ru phimmoi.net bandcamp.com letras.mus.br" +
        " fandango.com youtubeinmp3.com photobucket.com himado.in super.cz vidto.me pudelek.pl 1tv.ru mp3xd.com pixabay.com bs.to" +
        " prnt.sc tudou.com iheart.com last.fm ranker.com kinox.to pelispedia.tv kissmanga.com mangastream.com dmkt-sp.jp rutube." +
        "ru "
  },
  /**{
    "category": "Arts and Entertainment",
    "subCategory": "Animation and Comics",
    "urls": "mangafox.me mangahere.co mangareader.net readmanga.me kissmanga.com mangago.me dm5.com mangastream.com animeflv.net mangatown.com mangapanda.com dmzj.com webtoons.com jkanime.net gocomics.com tumangaonline.com anidub.com japscan.com senmanga.com comicbook.com mangahome.com readcomiconline.to 1kkk.com proxer.me mangahost.net animevost.org horriblesubs.info tonarinoyj.jp mangaeden.com dynasty-scans.com questionablecontent.net anipo.jp zerochan.net animeid.tv manga-zip.net explosm.net 77mh.com mangaz.com kingsmanga.net inkbunny.net smbc-comics.com mangaupdates.com shikimori.org dragonball-multiverse.com anime47.com goodfon.ru anime-planet.com oremanga.com turkanime.tv dl-zip.com",
  },
  {
    "category": "Arts and Entertainment",
    "subCategory": "Architecture",
    "urls": "archdaily.com plataformaarquitectura.cl architizer.com wankersearch.com freshome.com arq.com.mx professionearchitetto.it skyscraperpage.com gooood.hk archinect.com arkitera.com soloarquitectura.com castle.com arqhys.com contemporist.com decoist.com archweb.it vigilantcitizen.com arch2o.com archiexpo.es architectureartdesigns.com wikiarquitectura.com sagradafamilia.org roca.es architecturendesign.net miestai.net madeexpo.it architectes.org archpaper.com archiweb.cz aia.com.vn firstinarchitecture.co.uk lighthouseplz.com vitruvius.com.br 6sqft.com architettiroma.it archijob.co.il europaconcorsi.com heritageunits.com archiportale.com pritzkerprize.com arkiplus.com archbronconeumol.org arqa.com castlesncoasters.com bustler.net cleu.edu.mx compagnons-du-devoir.com aasarchitecture.com ocearch.org",
  },
  {
    "category": "Arts and Entertainment",
    "subCategory": "Awards",
    "urls": "shogakukan.co.jp nobelprize.org oscars.org regforum.ru webbyawards.com bafta.org pulitzer.org redcarpet-fashionawards.com tonyawards.com oscar.com junoawards.ca tvweeklogieawards.com.au writersofthefuture.com telly.com emmyonline.org concours-oscaro.com thehugoawards.org sakuraaward.com lovieawards.eu butlertrust.org.uk thebookmarks.co.za iemmys.tv poet-premium.ru worldmusicawards.com theedgars.com abcbyspu.com doveawards.com awardsandshows.com necatigil.com grawemeyer.org nyitawards.com a24awards.com videoawards.com wku-members.de manunuri.com leoawards.com bella-award.ru johnglover.com.au zimachievers.com myoscarpredictions.com dievinu.lt peacesymposium.org.uk pwnies.com dorothea.com.au cwmcawards.com centralmontanarcha.com haiku-hia.com latifaaward.org uonsu-uso.co.uk sovas.org",
  },
  {
    "category": "Arts and Entertainment",
    "subCategory": "Celebrities and Entertainment News",
    "urls": "pudelek.pl tmz.com super.cz ew.com jezebel.com zimbio.com digitalspy.com kapanlagi.com therichest.com hollywoodlife.com mdpr.jp purepeople.com justjared.com natalie.mu perezhilton.com deadline.com pagesix.com jeanmarcmorandini.com gala.fr topito.com dagospia.com wowkeren.com kstyle.com entretenimento.r7.com bossip.com news.nifty.com zakzak.co.jp virgula.uol.com.br alternatifim.com bollywoodlife.com spletnik.ru etonline.com ciudad.com.ar radaronline.com officiallyjd.com closermag.fr thevintagenews.com kanyetothe.com promiflash.de pausepeople.com hecklerspray.com lifeandstylemag.com public.fr extra.cz kozaczek.pl tochka.net tvnotas.com.mx toofab.com storm.mg betterfap.com ",
  },
  {
    "category": "Arts and Entertainment",
    "subCategory": "Fashion and Modeling",
    "urls": "highsnobiety.com fashion.ifeng.com maxmodels.pl fashionbeans.com whowhatwear.com stylebistro.com osinka.ru fashion.sohu.com thefashionspot.com fashionizers.com elle.co.jp elle.com.tw fashiony.ru vogue.com.tw wwd.com dietbook.biz thehunt.com businessoffashion.com lookbook.nu models.com purseblog.com fashion.sina.com.cn wmagazine.com cosmopolitan.lt siamok.com vogue.com.cn af-110.com fustany.com afterpay.com.au moteris.lt worldofbuzz.com perfecte.md niv.ru modelmanagement.com independentfashionbloggers.org magazine-data.com elle.nl fashiongonerogue.com bryanboy.com instyle.de panele.lt theblondesalad.com fashion-collect.jp thesartorialist.com wannabemagazine.com fashion.walla.co.il cupcakesandcashmere.com gqindia.com stylight.com fashionbank.ru ",
  },
  {
    "category": "Arts and Entertainment",
    "subCategory": "Humor",
    "urls": "9gag.com fanfiction.net pikabu.ru yaplakal.com diply.com fishki.net demotywatory.pl funnyjunk.com kwejk.pl joemonster.org cracked.com knowyourmeme.com todayhumor.co.kr xkcd.com thechive.com tickld.com joyreactor.cc break.com ebaumsworld.com cheezburger.com cuantarazon.com collegehumor.com dorkly.com trinixy.ru theonion.com somethingawful.com humoruniv.com download.filmfanatic.com fark.com thebrofessional.net roosterteeth.com niceoppai.net sadistic.pl dilbert.com alkislarlayasiyorum.com memecenter.com ahnegao.com.br jovemnerd.com.br mistrzowie.org pressa.tv fishki.pl mediatakeout.com 1jux.net thewrap.com bugaga.ru labaq.com adultswim.com guff.com qiushibaike.com besty.pl",
  },
  {
    "category": "Arts and Entertainment",
    "subCategory": "Movies",
    "urls": "imdb.com kinopoisk.ru filmweb.pl movie.douban.com subscene.com rottentomatoes.com allocine.fr my-hit.org fandango.com ivi.ru phimmoi.net repelis.tv kinox.to zalukaj.com pelispedia.tv movie2free.com metacritic.com mp3goo.com gnula.nu streamcomplet.com redbox.com newmovie-hd.com filmaffinity.com filmibeat.com mlsmatrix.com adorocinema.com divxtotal.com hdonline.vn kino-teatr.ru movie4k.to myegy.tv 720pizle.com hdfilmcehennemi.com mtime.com filmitorrent.org turkcealtyazi.org sensacine.com film.wp.pl yify-torrent.org doramatv.ru boxofficemojo.com torrentdosfilmes.com cima4u.tv xunlei.com movie-blog.org eiga.com omelete.uol.com.br movie4k.tv filmifullizle.org onlainfilm.ucoz.ua",
  },
  {
    "category": "Arts and Entertainment",
    "subCategory": "Music and Audio",
    "urls": "soundcloud.com youtube-mp3.org spotify.com convert2mp3.net pandora.com genius.com azlyrics.com discogs.com mp3.zing.vn ultimate-guitar.com mp3xd.com deezer.com musica.com vagalume.com.br last.fm worldstarhiphop.com mp3juices.cc music.yandex.ru nhaccuatui.com bandcamp.com zamunda.net letras.mus.br pitchfork.com iheart.com zaycev.net rutor.info cifraclub.com.br palcomp3.com biqle.ru xiami.com tunein.com aclst.com mr-jatt.com mp3indirdur.com chotot.com listentoyoutube.com billboard.com metrolyrics.com mixcloud.com suamusica.com.br elgenero.com iplayer.fm shareably.net songsterr.com lacuerda.net beatport.com ulub.pl musixmatch.com allmusic.com hotnewhiphop.com",
  },
  {
    "category": "Arts and Entertainment",
    "subCategory": "Performing Arts",
    "urls": "amctheatres.com nme.com sxsw.com infinitiads.com comedy-portal.net digitick.com broadway.com frontgatetickets.com stage.com infoconcert.com awin.com broadwayworld.com ticketland.ru goldstar.com adsplatform.com adsee.in cirquedusoleil.com tapjoyads.com atgtickets.com shiki.jp lbpiaccess.com biletiva.com comic-con.org siamdrama.com edhrec.com skiddle.com rodeohouston.com jegy.hu netdna-storage.com baydrama.com broadwaydirect.com wdl.org starnow.co.uk earwolf.com evenko.ca tdf.org roh.org.uk castingcallpro.com lacasting.com okepi.net mariinsky.ru adsalsaitalybranch.com nationaltheatre.org.uk ayads.co ticketmaster.se telecharge.com royalalberthall.com mizukinana.jp morris4x4center.com kennedy-center.org ",
  },
  {
    "category": "Arts and Entertainment",
    "subCategory": "Photography",
    "urls": "deviantart.com flickr.com shutterstock.com photobucket.com pixabay.com imgsrc.ru furaffinity.net weheartit.com 500px.com prnt.sc 123rf.com pexels.com shutterfly.com gettyimages.com dreamstime.com depositphotos.com istockphoto.com rus.ec unsplash.com nipic.com fotolia.com wallpaperscraft.com hizliresim.com fotor.com befunky.com alamy.com xitek.com lunapic.com pixieset.com ink361.com yupoo.com pixta.jp petapixel.com overleaf.com photoshelter.com photo-ac.com pixiz.com pho.to avatan.ru pizap.com favim.com kn3.net localmoxie.com tuchong.com fotolog.com gramblr.com fotocommunity.de bigstockphoto.com gallery.ru dlapk.mobi",
  },
  {
    "category": "Arts and Entertainment",
    "subCategory": "TV and Video",
    "urls": "youtube.com netflix.com wikia.com nicovideo.jp dailymotion.com xfinity.com vimeo.com hulu.com youku.com tubidy.mobi hotstar.com seasonvar.ru cda.pl dmkt-sp.jp youtu.be mediaset.it fmovies.to sky.com crunchyroll.com programme-tv.net hulu.jp nhk.or.jp gogoanime.io himado.in bs.to 1tv.ru tudou.com bfmtv.com rutube.ru lostfilm.tv vidto.me nationalgeographic.com myvidster.com nos.nl hbogo.com tf1.fr nrk.no dumpert.nl eonline.com keepvid.com tvguide.com uzone.id directv.com kanald.com.tr livetv.sx girlschannel.net aparat.com tnt-online.ru dr.dk abema.tv",
  },
  {
    "category": "Arts and Entertainment",
    "subCategory": "Visual Arts and Design",
    "urls": "pixiv.net behance.net deviantart.net creativemarket.com wallhaven.cc society6.com freelogoservices.com ezgif.com aryion.com nicoseiga.jp fineartamerica.com ac-illust.com vietdesigner.net cgsociety.org nastol.com.ua jibjab.com vectorstock.com clipartbest.com theoatmeal.com art.com tate.org.uk colourlovers.com saatchiart.com goodfon.su thisiscolossal.com clipartpanda.com abduzeedo.com owler.com pastelin.com gallerix.ru azcoloring.com publicdomainvectors.org toonator.com firsteleven.com canadagoose.com home-designing.com goseek.com widewalls.ch wyng.com wetcanvas.com hotspot.com artfinder.com concretejungle.ru paintingwithatwist.com mynamepix.com theartstory.org artmajeur.com editorsdepot.com fontsinuse.com greatbigcanvas.com",
  }*/
]
