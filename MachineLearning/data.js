import {Adult} from '../Crawlers/extracted-data/Adult/Adult';
import {ArtsAndEntertainment} from '../Crawlers/extracted-data/ArtsAndEntertainment/ArtsAndEntertainment'
import {AutosAndVehicles} from '../Crawlers/extracted-data/AutosAndVehicles/AutosAndVehicles';
import {BeautyAndFitness} from '../Crawlers/extracted-data/BeautyAndFitness/BeautyAndFitness';
import {Beauty} from '../Crawlers/extracted-data/BeautyAndFitness/Beauty';
import {BooksAndLiterature} from '../Crawlers/extracted-data/BooksAndLiterature/BooksAndLiterature';
import {BusinessAndIndustry} from '../Crawlers/extracted-data/BusinessAndIndustry/BusinessAndIndustry';
import {CareerAndEducation} from '../Crawlers/extracted-data/CareerAndEducation/CareerAndEducation';
import {ComputerAndElectronics} from '../Crawlers/extracted-data/ComputerAndElectronics/ComputerAndElectronics';
import {Finance} from '../Crawlers/extracted-data/Finance/Finance';
import {FoodAndDrink} from '../Crawlers/extracted-data/FoodAndDrink/FoodAndDrink';
import {Gambling} from '../Crawlers/extracted-data/Gambling/Gambling';
import {Games} from '../Crawlers/extracted-data/Games/Games';
import {Health} from '../Crawlers/extracted-data/Health/Health';
import {HomeAndGarden} from '../Crawlers/extracted-data/HomeAndGarden/HomeAndGarden';
import {InternetAndTelecom} from '../Crawlers/extracted-data/InternetAndTelecom/InternetAndTelecom';
import {LawAndGovernment} from '../Crawlers/extracted-data/LawAndGovernment/LawAndGovernment';
import {NewsAndMedia} from '../Crawlers/extracted-data/NewsAndMedia/NewsAndMedia';
import {PeopleAndSociety} from '../Crawlers/extracted-data/PeopleAndSociety/PeopleAndSociety';
import {PetsAndAnimals} from '../Crawlers/extracted-data/PetsAndAnimals/PetsAndAnimals';
import {RecreationAndHobbies} from '../Crawlers/extracted-data/RecreationAndHobbies/RecreationAndHobbies';
import {Reference} from '../Crawlers/extracted-data/Reference/Reference';
import {Science} from '../Crawlers/extracted-data/Science/Science';
import {Shopping} from '../Crawlers/extracted-data/Shopping/Shopping';
import {Sports} from '../Crawlers/extracted-data/Sports/Sports';
import {Travel} from '../Crawlers/extracted-data/Travel/Travel';

export const categories = [
  {
    name: 'Adult',
    data: Adult.split(','),
    subCategories: []
  }, {
    name: 'Arts and Entertainment',
    data: ArtsAndEntertainment.split(','),
    subCategories: []
  }, {
    name: 'Autos and Vehicles',
    data: AutosAndVehicles.split(','),
    subCategories: []
  }, {
    name: 'Beauty And Fitness',
    data: BeautyAndFitness.split(','),
    subCategories: [
      {
        name: 'Beauty',
        data: Beauty.split(',')
      }
    ]
  }, {
    name: 'Books and Literature',
    data: BooksAndLiterature.split(','),
    subCategories: []
  }, {
    name: 'Business and Industry',
    data: BusinessAndIndustry.split(','),
    subCategories: []
  }, {
    name: 'Career and Education',
    data: CareerAndEducation.split(','),
    subCategories: []
  }, {
    name: 'Computer and Electronics',
    data: ComputerAndElectronics.split(','),
    subCategories: []
  }, {
    name: 'Finance',
    data: Finance.split(','),
    subCategories: []
  }, {
    name: 'Food and Drink',
    data: FoodAndDrink.split(','),
    subCategories: []
  }, {
    name: 'Gambling',
    data: Gambling.split(','),
    subCategories: []
  }, {
    name: 'Games',
    data: Games.split(','),
    subCategories: []
  }, {
    name: 'Health',
    data: Health.split(','),
    subCategories: []
  }, {
    name: 'Home and Garden',
    data: HomeAndGarden.split(','),
    subCategories: []
  }, {
    name: 'Internet and Telecom',
    data: InternetAndTelecom.split(','),
    subCategories: []
  }, {
    name: 'Law and Government',
    data: LawAndGovernment.split(','),
    subCategories: []
  }, {
    name: 'News and Media',
    data: NewsAndMedia.split(','),
    subCategories: []
  }, {
    name: 'People and Society',
    data: PeopleAndSociety.split(','),
    subCategories: []
  }, {
    name: 'Pets and Animals',
    data: PetsAndAnimals.split(','),
    subCategories: []
  }, {
    name: 'Recreation and Hobbies',
    data: RecreationAndHobbies.split(','),
    subCategories: []
  }, {
    name: 'Reference',
    data: Reference.split(','),
    subCategories: []
  }, {
    name: 'Science',
    data: Science.split(','),
    subCategories: []
  }, {
    name: 'Shopping',
    data: Shopping.split(','),
    subCategories: []
  }, {
    name: 'Sports',
    data: Sports.split(','),
    subCategories: []
  }, {
    name: 'Travel',
    data: Travel.split(','),
    subCategories: []
  }
]
