import natural from 'natural';
import {classifierData} from './classifier';
import {_} from 'meteor/underscore';

let classifier = natural.BayesClassifier.restore(classifierData);

Meteor.startup(function() {
  if (Meteor.isServer) {
    Meteor.methods({
      predictCategory: function(input) {
        //let test = classifier.getClassifications('Vacations');
        let result = [];
        console.log(input);
        if (input.name) {
          let resultName = classifier.classify(input.name);
          result.push(resultName);
        }

        if (input.description) {
          let resultDescription = classifier.classify(input.description);
          result.push(resultDescription);
        }

        if (input.keywords)
          input.keywords.forEach((k) => {
            result.push(classifier.classify(k));
          })


        console.log(result);
        console.log(mostRelevant(result))

        return mostRelevant(result);
      }
    });
  }
});

mostRelevant = (arr) => {
  return arr.sort((a, b) => arr.filter(v => v === a).length - arr.filter(v => v === b).length).pop();
}
