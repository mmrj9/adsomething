import LanguageDetect from 'languagedetect';
import cheerio from 'cheerio';
import request from 'request';
import _ from 'underscore';
import fs from 'fs';

//Local imports
import {data} from './data/Travel/Travel';

let finalResult = [];
let lngDetector = new LanguageDetect();

_.each(data, (elem, index) => {
  let {category, subCategory, urls} = elem;
  console.log(`Starting category ${category} > ${subCategory}`);
  urls = urls.split(" ");
  urls.forEach(function(url) {
    console.log(`Crawling ${url}`);
    let uri = `http://${url}`
    request(uri, function(error, response, html) {
      if (!error && response.statusCode == 200) {
        let $ = cheerio.load(html);
        let meta = $('meta');
        let text = [];
        /*    $('h1,h2,h3,h4,h5,p,span').each(function(index, element) {
          let lngDetector = new LanguageDetect();
          if (element.children[0] && element.children[0].data && lngDetector.detect(element.children[0].data)[0] && lngDetector.detect(element.children[0].data)[0][0] == 'english') {
            text.push(element.children[0].data.trim());
          }
        });*/
        let title = $("title").text();
        let keys = Object.keys(meta);
        let metaTags = {
          name: title || '',
          description: '',
          keywords: [],
          text: text || []
        }
        keys.forEach(function(key) {
          if (meta[key].attribs && meta[key].attribs.name) {
            if (meta[key].attribs.name == 'description')
              metaTags.description = meta[key].attribs.content;
            else if (meta[key].attribs.name == 'keywords' && meta[key].attribs.content)
              metaTags.keywords = meta[key].attribs.content.split(",");
            }
          });

        let lngDetector = new LanguageDetect();
        let descriptionDetection = lngDetector.detect(metaTags.description);
        let nameDetection = lngDetector.detect(metaTags.name);
        //    if ((nameDetection[0] && nameDetection[0][0] == 'english') || (descriptionDetection[0] &&
        // descriptionDetection[0][0] == 'english')) {
        _.each(metaTags, (tag) => {
          if (tag) {
            if (tag.constructor === Array) {
              tag.forEach(function(k) {
                let result = k.trim().replace(/"|,|(?:\r\n|\r|\n)/g, '');
                finalResult.push(result);
              })
            } else {
              let result = tag.trim().replace(/"|,|(?:\r\n|\r|\n)/g, '');
              finalResult.push(result);
            }
          }
        });
        console.log(`Crawled ${url} successfully`);
        return;
        /*  } else {
          console.log(`${url} is not in english`)
        }*/
      } else {
        console.log(`Couldn't crawl ${url}`);
        return null;
      }
    })
  });
});

// content of index.js
const http = require('http')
const port = 3000

const requestHandler = (request, res) => {
  let uniqueArray = finalResult.filter(function(item, pos) {
    return finalResult.indexOf(item) == pos;
  })
  uniqueArray.sort();
  res.writeHead(200, {
    'Content-Type': 'application/force-download',
    'Content-disposition': 'attachment; filename=data.txt'
  });
  res.end(uniqueArray.toString());
  /**res.write(finalResult.join(""));
res.end();*/
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
})
