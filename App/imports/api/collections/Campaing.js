import {Mongo} from 'meteor/mongo';

export const Campaings = new Mongo.Collection('campaings');

Meteor.startup(function() {
    if (Meteor.isServer) {
        Meteor.methods({
            'createCampaing': function(campaing) {
                return Campaings.insert(campaing);
            }
        });
    }
});
