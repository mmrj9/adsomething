export const data = [
  {
    "category": "Beauty and Fitness",
    "urls": "boredpanda.com livestrong.com bodybuilding.com myfitnesspal.com sephora.com beauty.hotpepper.jp cosme.net ulta.com mensh" +
        "ealth.com oriflame.com beauty.yahoo.co.jp skincare-univ.com my-personaltrainer.it wizaz.pl flexmls.com women.kapook.com " +
        "ipsy.com weightwatchers.com prevention.com greatist.com natura.net mensfitness.com bathandbodyworks.com shape.com avon.r" +
        "u sparkpeople.com crossfit.com stylecraze.com muscleandfitness.com avon.com.br fitnessmagazine.com fragrantica.com colou" +
        "rpop.com sephora.fr smartfren.com shiseido.co.jp boticario.com.br kosmetista.ru sallybeauty.com makeupalley.com makeup.c" +
        "om.ua herbeauty.co natura.com.br dailyburn.com beautybay.com skinhub.com beachbody.com loccitane.com eva.ro jeban.com ma" +
        "ccosmetics.com lush.com teambeachbody.com fragrancenet.com totalbeauty.com vitalia.pl thehealthsite.com birchbox.com sti" +
        "tchfix.com cliomakeup.com makeupandbeauty.com kikocosmetics.com fitnessblender.com beaute-test.com planetfitness.com too" +
        "faced.com belezanaweb.com.br feelunique.com fragrantica.ru youniqueproducts.com tartecosmetics.com letu.ru yves-rocher.r" +
        "u morphebrushes.com aroma-zone.com fitforfun.de lookfantastic.com muscleandstrength.com elfcosmetics.com naturallycurly." +
        "com nerdfitness.com lorealparisusa.com temptalia.com rivegauche.ru xovain.com iledebeaute.ru kao.com dove.com fashiongui" +
        "de.com.tw yogajournal.com strawberrynet.com zumba.com money.bhaskar.com nyxcosmetics.com beautylish.com mycharm.ru minq." +
        "com calorieking.com staggeringbeauty.com lushusa.com "
  },
  /*{
    "category": "Beauty and Fitness",
    "subCategory": "Beauty",
    "urls": "beauty.hotpepper.jp avon.ru ipsy.com skincare-univ.com beauty.yahoo.co.jp wizaz.pl sephora.fr women.kapook.com fragranti" +
        "ca.com fragrancenet.com totalbeauty.com lush.com stitchfix.com herbeauty.co toofaced.com birchbox.com eva.ro beaute-test" +
        ".com tartecosmetics.com minq.com yves-rocher.fr jeban.com byrdie.com beautylish.com cultbeauty.co.uk kao.com nyxcosmetic" +
        "s.com lrworld.com nocibe.fr fashionguide.com.tw urcosme.com stylist.co.uk dove.com xovain.com mycharm.ru canmake.com all" +
        "beauty.com neutrogena.com hermo.my birchbox.fr beautypedia.com staggeringbeauty.com essence.eu paradisi.de bareminerals." +
        "com purplle.com loreal-paris.es beautyexchange.com.hk i-voce.jp parfumo.de "
  }, {
    "category": "Beauty and Fitness",
    "subCategory": "Bodyart",
    "urls": "flexmls.com smartfren.com inkedmag.com tattoodo.com mundodastatuagens.com.br zonatattoos.com tattoo.com bme.com tattoode" +
        "sign.com lead-alliance.net tattoo-models.net tattooers.net tattoome.com makarem.ir createmytattoo.com tattoobite.com tat" +
        "toofontgenerator.net tattoo-bewertung.de hennacolorlab.com dubuddha.org trucosface.com tattoo-ideas.com artenocorpo.com " +
        "tattoojohnny.com tattooimages.biz lojadatatuagem.com.br kingpintattoosupply.com tattoomarket.ru tatuirovanie.ru bigtatto" +
        "oplanet.com tatuarte.org tattly.com worldwidetattoo.com facezbok.pl tattoostime.com news.bme.com tattoosme.com worldtatt" +
        "ooevents.com tattooscout.de tattook.ru skin-artists.com stylendesigns.com tattootribes.com electricinkonline.com.br itcp" +
        "iercing.com killerinktattoo.fr tattoosuperstore.com intenzetattooink.com tatuajesfemeninos.com tattoomachineequipment.co" +
        "m "
  }, {
    "category": "Beauty and Fitness",
    "subCategory": "Cosmetics",
    "urls": "sephora.com oriflame.com cosme.net natura.net colourpop.com avon.com.br boticario.com.br shiseido.co.jp beautybay.com ma" +
        "ccosmetics.com kosmetista.ru natura.com.br letu.ru loccitane.com youniqueproducts.com kikocosmetics.com rivegauche.ru be" +
        "lezanaweb.com.br yves-rocher.ru morphebrushes.com fragrantica.ru elfcosmetics.com iledebeaute.ru lorealparisusa.com lush" +
        "usa.com temptalia.com sephora.com.br fragrancex.com clinique.com sephora.pl maybelline.com catalog-avon.ru sephora.com.a" +
        "u aoro.ro marykay.com jamberry.com basenotes.net letoile.ru benefitcosmetics.com jolse.com sephora.it makeupgeek.com mec" +
        "ca.com.au urbandecay.com yves-rocher.com maccosmetics.co.uk katvondbeauty.com avonrepresentative.com marykay.com.br cove" +
        "rgirl.com "
  }, {
    "category": "Beauty and Fitness",
    "subCategory": "Fitness",
    "urls": "bodybuilding.com menshealth.com prevention.com crossfit.com my-personaltrainer.it sparkpeople.com mensfitness.com shape." +
        "com greatist.com fitnessmagazine.com muscleandfitness.com fitnessblender.com beachbody.com vitalia.pl planetfitness.com " +
        "dailyburn.com zumba.com money.bhaskar.com yogajournal.com 24hourfitness.com muscleandstrength.com roguefitness.com fitfo" +
        "rfun.de puregym.com lafitness.com nerdfitness.com hipertrofia.org equinox.com treinomestre.com.br smartfit.com.br fit4br" +
        "ain.com efitness.com.pl exrx.net precisionnutrition.com acefitness.org nowloss.com jv.ru diets.ru lifetimefitness.com st" +
        "ack.com stronglifts.com anytimefitness.com thegymgroup.com tvoytrener.com missfit.ru smaregi.jp dietadiary.com basic-fit" +
        ".com shop.builder.hu nasm.org "
  }, {
    "category": "Beauty and Fitness",
    "subCategory": "Hair",
    "urls": "naturallycurly.com latest-hairstyles.com samsbeauty.com wigtypes.com hairfinder.com blackgirllonghair.com cutegirlshairs" +
        "tyles.com ukhairdressers.com paulayoung.com longhaircommunity.com haircrazy.com mylocalsalon.com divatress.com voguewigs" +
        ".com hqhair.com wigs.com elevatestyles.com 24hair.ru hairstyleonpoint.com shop.wigsbuy.com schwarzkopf.com hairsisters.c" +
        "om thehairstyler.com matrix.com recuperarelpelo.com hairfinity.com especiallyyours.com hairtobeauty.com stylesweekly.com" +
        " blackhairmedia.com maxxhair.vn hairstopandshop.com pantene.com folica.com blackhairinformation.com carolsdaughter.com h" +
        "airbuddha.net haircuttery.com uniwigs.com toniandguy.com evawigs.com evahair.com wigsmart.co.uk bumbleandbumble.com wen." +
        "com fridaynighthair.com johnfrieda.com alopezie.de vivicafoxhair.com hairdirect.com "
  }, {
    "category": "Beauty and Fitness",
    "subCategory": "Skin Care",
    "urls": "skinhub.com arbonne.com paulaschoice.com kiehls.com esteelauder.com massageenvy.com skincaretalk.com annmariegianni.com " +
        "origins.com skinkings.com makeup.com essentialdayspa.com truthinaging.com murad.com skinacea.com lifecellskin.com itsski" +
        "n.com clarisonic.com paulaschoice.com.au laroche-posay.us skinfine.ru kiehls.ca proactiv.co.uk cosmeticscop.com dermalog" +
        "ica.com tatcha.com elementsmassage.com skinvision.com skincarerx.com nivea.fr urbanskinrx.com paulaschoice.com.tw dermae" +
        ".com skin.pt bona-fide-skincare.com makeupartistschoice.com cerave.com perriconemd.com aveneusa.com uncover-skincare.nl " +
        "clearskinforever.net drbaileyskincare.com activeskin.com.au bihakuskincarenavi.com paulaschoice.co.uk timelessha.com sim" +
        "pleskincare.com effortlessskin.com thepureskin.tumblr.com tuelskincare.com "
  }, {
    "category": "Beauty and Fitness",
    "subCategory": "Weight Loss",
    "urls": "weightwatchers.com nutrisystem.com myproana.com slism.jp calorieking.com weightwatchers.de celebrityweightloss.com islim" +
        "ming.ru boohee.com dietspotlight.com weightlossresources.co.uk weightwatchers.ca bmi-rechner.net calorie.it tsfl.com wei" +
        "ghtwatchers.co.uk 3weekdiet.com maigriravecsatete.com beyonddiet.com abnehmen.com azbukadiet.ru idealshape.com fatsecret" +
        ".de thefastdiet.co.uk weightwatchers.com.au dhzw.org noom.com coolsculpting.com robbwolf.com dukandiet.ru fatsthatfightf" +
        "at.com gameofweightloss.com ketogenic-diet-resource.com thenewyouplan.com idealprotein.com stroiniashka.ru weightwatcher" +
        "s.be hydroxycut.com no1-diet.com mirimanova.ru lowcarb-ernaehrung.info vucutgelistirmeteknikleri.com exercise4weightloss" +
        ".com truweight.in peertrainer.com healthyweightforum.org skinnypoints.com height-weight-chart.com cb1weightgainer.com em" +
        "ail-weightwatchers.com "
  }*/
]

/**
{
  "category": "",
  "subCategory": "",
  "urls": "",
}
*/
