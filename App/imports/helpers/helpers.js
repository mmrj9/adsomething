/*
  TODO - Organize helper methods per file
 */

valueToCurrency = (value) => {
    return `${value}€`;
}

currencyToValue = (currency) => {
    return parseFloat(currency.replace('€', ''));
}

toDecimal = (value) => {
    if (value.toString().indexOf('.') == -1)
        return value.toString().concat('.0');
    else if (decimalPlaces(value) > 2)
        return value.toFixed(2);
    else
        return value;
    }

decimalPlaces = (num) => {
    return (num.toString().split('.')[1] || []).length;
}

addDays = (date, days) => {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

addYears = (date, years) => {
    let newDate = date.setFullYear(date.getFullYear() + years);
    return new Date(newDate);
}
