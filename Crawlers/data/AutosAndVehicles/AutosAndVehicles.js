export const data = [
  {
    "category": "Autos and Vehicles",
    "urls": "drom.ru autohome.com.cn drive2.ru mobile.de auto.mail.ru otomoto.pl cargurus.com auto.ru auto.drom.ru autotrader.co.uk j" +
      "alopnik.com indianrail.gov.in autotrader.com flightradar24.com oreillyauto.com kbb.com autozone.com carview.co.jp cars.c" +
      "om autoscout24.de edmunds.com forocoches.com xcar.com.cn caranddriver.com dmv.org auto.ria.com motorsport.com ford.com r" +
      "zd.ru carfax.com carsensor.net nationalrail.co.uk pcauto.com.cn webmotors.com.br clickmon.co.kr auto.sohu.com autoblog.c" +
      "om motor-talk.de rentalcars.com goo-net.com honda.com toyota.com carid.com letgo.com rockauto.com lacentrale.fr autoscou" +
      "t24.it hasznaltauto.hu summitracing.com enterprise.com rst.ua carmax.com click.uol.com.br forum-auto.com formula1.com ca" +
      "r.gr icarros.com.br mobile.bg carsales.com.au carwale.com nascar.com adayroi.com blablacar.fr toyota.jp pistonheads.com " +
      "advanceautoparts.com kia.com emex.ru autoplius.lt autobild.de moto.wp.pl truecar.com auto.ifeng.com zr.ru cardekho.com b" +
      "lamper.ru caradisiac.com chevrolet.com volvocars.com auto.onliner.by auto.163.com coches.net autos.mercadolibre.com.ar o" +
      "scaro.com arabam.com auto.qq.com tirerack.com totalcar.hu motortrend.com yamaha.com autovit.ro carsforsale.com honda.co." +
      "jp kolesa.kz bitauto.com moto.onet.pl jappy.com shop.advanceautoparts.com erail.in hertz.com "
  }
  /**  {
    "category": "Autos and Vehicles",
    "subCategory": "Automotive Industry",
    "urls": "autozone.com oreillyauto.com mobile.bg rockauto.com goo-net.com carwale.com dmv.org summitracing.com emex.ru advanceautoparts.com porsche.com oscaro.com av.by tirerack.com autodoc.ru abw.by partsgeek.com japancats.ru aramisauto.com response.jp auto.onliner.by autoanything.com lada.ru hyundai.com vezess.hu news.drom.ru autopiter.ru mister-auto.com epcdata.ru subaru.jp nissan.co.jp kfzteile24.de oponeo.pl kolesa-darom.ru norauto.fr audi.de auto.tut.by pepboys.com auto-data.net toyota.co.jp ferrari.com avtobazar.ua euroauto.ru daimler.com suzuki.co.jp discounttire.com kia.ru feuvert.fr njcar.ru reifendirekt.de",
  },
  {
    "category": "Autos and Vehicles",
    "subCategory": "Automotive News",
    "urls": "drive2.ru autobild.de auto.ifeng.com motor-talk.de pistonheads.com zr.ru motor.ru motortrend.com autoexpress.co.uk topgear.com hemmings.com team-bhp.com drive.ru auto-motor-und-sport.de avtovzglyad.ru carthrottle.com vwvortex.com autoplus.fr drivespark.com moto.pl paultan.org auto-moto.com auto.vesti.ru autozeitung.de planetf1.com autoweek.com clublexus.com hotrod.com gtplanet.net autoua.net flatout.com.br pddmaster.ru bmwclub.ru noticiasautomotivas.com.br vroom.be autoreview.ru motorsport-magazin.com vwts.ru carmagazine.co.uk hyundai-forums.com motorpage.ru car.ru superstreetonline.com e46fanatics.com 110km.ru zarulem.ws odometer.com corral.net auto.ironhorse.ru indycar.com",
  },
  {
    "category": "Autos and Vehicles",
    "subCategory": "Aviation",
    "urls": "flightradar24.com airliners.net hangngon.net jetphotos.com pwr.wroc.pl boeing.com x-plane.org flytothesky.ru planespotters.net controller.com planetakino.ua liveatc.net avherald.com flyingtiger.com frankfurt-airport.com romanticflyers.ru twentyonepilots.com aircraftspruce.com helipal.com flyingmag.com lmco.com airnav.com icao.int kokpit.aero trade-a-plane.com alliedpilots.org airbase.ru paraplan.ru flyingv.cc txtav.com aviationexam.com airliners.de partsbase.com faasafety.gov airspacemag.com boldmethod.com esky.com.tr f-16.net fltplan.com foreflight.com flygermania.com misterfly.com skybrary.aero eaa.org aerotrader.com globalair.com s4a.aero balloontime.com pilotworkshop.com flugzeugforum.de",
  },
  {
    "category": "Autos and Vehicles",
    "subCategory": "Boating",
    "urls": "boatingmag.tv jlc.ne.jp iboats.com boats.net ybw.com overtons.com woodenboat.com defender.com hisse-et-oh.com theyachtmarket.com katera.ru boatdesign.net sailnet.com boatus.com cruisingworld.com nauticexpo.com boat24.com boatshop24.com meanchicken.net boatwizard.com apolloduck.co.uk yacht.de sailboatdata.com uboat.net yachtall.com glen-l.com best-boats24.net canalrivertrust.org.uk boot24.com getmyboat.com boat-fuel-economy.com northsails.com boattest.com yachtingmagazine.com 24hourshowroom.com boaterexam.com shom.fr rangerboats.com voilesetvoiliers.com go2marine.com noonsite.com yachtingworld.com ffvoile.fr discoverboating.com nauticexpo.fr duckworksbbs.com denisonyachtsales.com duckworksmagazine.com elwis.de segeln-forum.de",
  },
  {
    "category": "Autos and Vehicles",
    "subCategory": "Car Buying",
    "urls": "drom.ru autohome.com.cn mobile.de autotrader.co.uk auto.drom.ru autotrader.com auto.ria.com autoscout24.de kbb.com cars.com hasznaltauto.hu edmunds.com rst.ua car.gr carsensor.net autoscout24.it lacentrale.fr carmax.com letgo.com jappy.com carsales.com.au kolesa.kz blablacar.fr truecar.com myauto.ge adayroi.com autoscout24.ch cars.bg arabam.com auto.bazos.cz clickmon.co.kr autotrader.ca autovit.ro copart.com cardekho.com auto.e1.ru largus.fr polovniautomobili.com click.uol.com.br beforward.jp che168.com carsales.mobi auto24.ee autoscout24.be chileautos.cl rvtrader.com parkers.co.uk autobazar.sk zigwheels.com one2car.com ",
  },
  {
    "category": "Autos and Vehicles",
    "subCategory": "Car Rentals",
    "urls": "rentalcars.com enterprise.com hertz.com avis.com budget.com timescar.jp blablacar.pl blablacar.de blablacar.es olacabs.com carrentals.com alamo.com zipcar.com sixt.com billiger-mietwagen.de nationalcar.com nipponrentacar.co.jp sixt.de europcar.com zoomcar.com sniperhire.net dollar.com drivy.com zuche.com car2go.com cartrawler.com thrifty.com enterprise.co.uk rvshare.com localiza.com economycarrentals.com amovens.com blablacar.com zuzuche.com holidayautos.com sixt.fr enterprise.ca suntransfers.com budgettruck.com europcar.de europcar.co.uk europcar.fr arguscarhire.com ouicar.fr goldcar.es hertz.fr sixt.co.uk economybookings.com avis.co.uk movida.com.br ",
  },
  {
    "category": "Autos and Vehicles",
    "subCategory": "Makes and Models",
    "urls": "ford.com toyota.com honda.com volvocars.com chevrolet.com kia.com landrover.com bmwusa.com mbusa.com nissanusa.com netcarshow.com audiusa.com vw.com mazdausa.com mercedes-benz.com jeep.com subaru.com hyundaiusa.com gm.com ford-trucks.com toyotafinancial.com buyatoyota.com dodge.com jaguar.com audi.com lexus.com renault.fr chrysler.com renault.com peugeot.com renault.ru fiat.it japancar.ru cadillac.com peugeot.fr honda.com.tr renault.com.tr honda.ca volkswagen.co.uk acura.com gmc.com fiat.com toyota.ru volkswagen.fr bentleymotors.com bmw.com ford.ru citroen.com nasioc.com lexus.jp",
  },
  {
    "category": "Autos and Vehicles",
    "subCategory": "Motorcycles",
    "urls": "revzilla.com honda2wheelersindia.com goobike.com yamaha-motor.eu harley-davidson.com moto.it cycletrader.com bikewale.com motorcyclistonline.tv louis.de fc-moto.de motorkari.cz bikebros.co.jp jpcycles.com bikedekho.com moto.mercadolivre.com.br motorcyclenews.com cycleworld.com royalenfield.com ktm.com yamaha-motor.com.br motorcycle-superstore.com hdforums.com yamaha-motor.co.jp polo-motorrad.de motoblouz.com motorradonline.de motoclub-tingavert.it bikesales.com.au motosiklet.net bajajauto.com kawasaki.com bikebandit.com vitalmx.com brp.com dueruote.it nettimoto.com ducati.com bikepost.ru denniskirk.com moto.com.br insella.it lerepairedesmotards.com ridertv.com la-becanerie.com motoservices.com partzilla.com sportsbikeshop.co.uk motorcycle.com moto-station.com",
  },
  {
    "category": "Autos and Vehicles",
    "subCategory": "Motorsports",
    "urls": "motorsport.com nascar.com formula1.com motogp.com gpupdate.net autosport.com motorsport-total.com f1-gate.com f1i.com as-web.jp pirate4x4.com grandepremio.uol.com.br racing.hkjc.com crash.net f1today.net formel1.de yellowbullet.com thumpertalk.com drift.com mclaren.com wrc.com f1technical.net racerxonline.com espnf1.com gpone.com rallye-magazin.de ewrc-results.com worldsbk.com nhra.com supercars.com suzukacircuit.jp formula55.tj totalmotorcycle.com circuit.co.uk racecarthings.com nuerburgring.de motociclismo.es autosport.pt sportmotor.hu jamesallenonf1.com gp-inside.com jayski.com f-1.lt news.webike.net racing.nextmedia.com grandprix.com.au racing-reference.info formule1.nl ewrc.cz rallye-sport.fr",
  },
  {
    "category": "Autos and Vehicles",
    "subCategory": "Trains and Railroads",
    "urls": "indianrail.gov.in rzd.ru nationalrail.co.uk erail.in oebb.at etrain.info belgianrail.be drehscheibe-online.de bkk.hu trainstatus.info vr.fi westjr.co.jp trainman.in mbta.com realtimetrains.co.uk trains.com railenquiry.in idtgv.com railforums.co.uk etrains.in delhimetrorail.com steamid.eu railpictures.net huoche.net trainpix.org trainorders.com networkrail.co.uk zelpage.cz parovoz.com ferrovie.it 16-25railcard.co.uk maerklin.de rfi.it bahnbilder.de vagonweb.cz rrpicturearchives.net zsr.sk delhimetrorail.info ogaugerr.com railroad.net surutto.com mytrainticket.co.uk srps.org.uk railway.gov.lk irfca.org trainspy.com saudirailways.org railway.ge sinetram.com.br ldz.lv",
  },*/
]
