export const data = [
  {
    "category": "Food and Drink",
    "urls": "cookpad.com tabelog.com allrecipes.com hotpepper.jp zomato.com food.com foodnetwork.com gnavi.co.jp chefkoch.de dominos." +
      "com marmiton.org mcdonalds.com pizzahut.com tudogostoso.com.br thekitchn.com giallozafferano.it russianfood.com retty.me" +
      " asda.com kroger.com dianping.com starbucks.com yemeksepeti.com seriouseats.com papajohns.com opentable.com bbcgoodfood." +
      "com povarenok.ru delish.com nefisyemektarifleri.com grubhub.com just-eat.co.uk epicurious.com bettycrocker.com tasteofho" +
      "me.com sainsburys.co.uk eater.com foody.vn chowhound.com 750g.com idhad.com cuisineaz.com povar.ru ovkuse.ru jamieoliver" +
      ".com edimdoma.ru kwestiasmaku.com pcplus.ca zhenskoe-mnenie.ru ah.nl smaker.pl taste.com.au safeway.com yemek.com subway" +
      ".com myrecipes.com woolworths.com.au bonappetit.com starbucks.co.jp openrice.com tacobell.com nespresso.com suntory.co.j" +
      "p panerabread.com kraftrecipes.com simplyrecipes.com xiachufang.com ekiten.jp eda.ru nosalty.hu aldi-sued.de seamless.co" +
      "m mangaku.web.id foodandwine.com food52.com coles.com.au publix.com kaufland.de mcdonalds.co.jp koolinar.ru damndeliciou" +
      "s.net blueapron.com cafeblog.hu eatingwell.com aldi.us 82cook.com ubereats.com kochbar.de dominos.co.uk jesselanewellnes" +
      "s.com pillsbury.com femmes.orange.fr meishij.net lakii.com coolinarika.com morrisons.com olivegarden.com leclercdrive.fr" +
      " wholefoodsmarket.com misya.info "
  }
]
