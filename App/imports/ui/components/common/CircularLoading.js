import React, {Component} from 'react';
import CircularProgress from 'material-ui/CircularProgress';

export default class CircularLoading extends Component {
  render() {
    return (
      <div style={styles.screenOverlay}>
          <CircularProgress size={80} thickness={5}/> {this.props.text
            ? <h5>{this.props.text}</h5>
            : <div></div>}
          <span>{'Please wait'}</span>
      </div>
    );
  }
}

const styles = {
  screenOverlay: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(255,255,255,0.8)',
    left: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    zIndex: 2
  }
}
