export const data = [
  {
    "category": "Books and Literature",
    "urls": "scribd.com goodreads.com archiveofourown.org wattpad.com issuu.com audible.com pensador.uol.com.br quotev.com tvtropes.o" +
        "rg jianshu.com barnesandnoble.com wikibooks.org dummies.com jstor.org overdrive.com spaggiari.eu sagepub.com qidian.com " +
        "labirint.ru asianfanfics.com litres.ru scholastic.com casadellibro.com samlib.ru livelib.ru stihi.ru kitapyurdu.com book" +
        "depository.com abebooks.com biografiasyvidas.com safaribooksonline.com hatenadiary.jp packtpub.com vitalsource.com antol" +
        "oji.com lubimyczytac.pl poetryfoundation.org pottermore.com tdk.gov.tr litra.ru cengagebrain.com idefix.com honto.jp tai" +
        "lieu.vn briefly.ru gutenberg.org thepointsguy.com oreilly.com kobo.com proza.ru bookmeter.com calibre-ebook.com skoob.co" +
        "m.br cairn.info hatenadiary.com babelio.com fantasy-worlds.org poemas-del-alma.com book118.com ebook3000.com elibrary.ru" +
        " galegroup.com poemhunter.com indigo.ca alice.it christianbook.com familyfriendpoems.com thalia.de poets.org zongheng.co" +
        "m audible.co.uk litmir.me scp-wiki.net litcharts.com buecher.de bartleby.com siir.gen.tr fantlab.ru chitai-gorod.ru book" +
        "walker.jp ebookjapan.jp nap.edu kekanto.com.br knijky.ru kitapsec.com translationnations.com efpfanfic.net abebooks.co.u" +
        "k foboko.com tradepub.com apastyle.org lafeltrinelli.it bookz.ru diary.to recantodasletras.com.br baka-tsuki.org toronto" +
        "publiclibrary.ca freebookspot.es thriftbooks.com rupoem.ru "
  }
  // , {   "category": "Books and Literature",   "subCategory": "Book Retailers",   "urls": "audible.com barnesandnoble.com
  // labirint.ru dummies.com sagepub.com kitapyurdu.com scholastic.com bookdepository.com abeb" +       "ooks.com
  // christianbook.com tdk.gov.tr indigo.ca thalia.de galegroup.com cengagebrain.com zinio.com thriftbooks.com gylde" +
  //    "ndal.dk booklooker.de buecher.de kitapsec.com nadirkitap.com tradepub.com adlibris.com bkstr.com
  // booktopia.com.au bonito" +       ".pl waterstones.com alibris.com intechopen.com blurb.com bookline.hu libraccio.it
  // mann-ivanov-ferber.ru zvab.com sanmin." +       "com.tw okuoku.com editorajuspodivm.com.br libri.hu booksamillion.com
  // libreriauniversitaria.it betterworldbooks.com prosv" +       ".ru libris.ro bookclub.ua biblio.com textbooks.com
  // travessa.com.br doverpublications.com znak.com.pl " }, {   "category": "Books and Literature",   "subCategory": "E
  // Books",   "urls": "wattpad.com vitalsource.com safaribooksonline.com packtpub.com honto.jp gutenberg.org elibrary.ru
  // kobo.com tailieu.vn eb" +       "ookjapan.jp nap.edu litmir.me calibre-ebook.com freebookspot.es book-online.com.ua
  // briefly.ru smashwords.com book118.com" +       " free-ebooks.net baka-tsuki.org ebrary.com libraryreserve.com koob.ru
  // mexalib.com bookz.ru popo.tw litlib.net bookboon.c" +       "om bookrix.com book-secure.com coollib.net ebookee.org
  // audiobooks.com kobobooks.com tululu.org ebooks.edu.gr bookmate.co" +       "m manybooks.net readanybook.com grin.com
  // qisuu.com txt99.com kodges.ru ebookdz.com bookrix.de general-ebooks.com wolnele" +       "ktury.pl modernlib.ru
  // ebooks-gratuit.com planetebook.com " }, {   "category": "Books and Literature",   "subCategory": "Folklore",
  // "urls": "pensieriparole.it detskiychas.ru vseskazki.su rhymes.org.uk americanfolklore.net leyendas-urbanas.com
  // gwinnettpl.org ilo" +       "vewerewolves.com mitos-mexicanos.com aforismario.it hyaenidae.narod.ru
  // childrensministry.com iita.org surlalunefairytale" +       "s.com telugubhakti.com folklore.ee terrenouvelle.ca
  // jdlf.com morfars.dk lnb.lt auditoriaswireless.net blat.dp.ua oldruss" +       "ia.net sayings.ru fsgw.org
  // rusfolklor.ru refranesyfrases.com skazki.org.ru fsjnet.jp lemotdelasemaine.com magie-et-fanta" +       "sy.com
  // folklornisdruzeni.cz folklore.mx folkloredebiyat.org lafrasegiusta.it mentorsroom.com ysopet.free.fr folklorerosa" +
  //      "rio.com.ar grimm01.de folklore-bg.com haikupoetshut.com rp5.com.ua westcountryfolklore.blogspot.co.uk
  // simplyaustralia.ne" +       "t haitianproverbs.com gigalis.org talebook.ru westernfolklore.org museumofhoaxes.com
  // fabulasurbanas.com " }, {   "category": "Books and Literature",   "subCategory": "Guides and Reviews",   "urls":
  // "biografiasyvidas.com zongheng.com 1000kitap.com kekanto.com.br allpoetry.com dechile.net booknode.com lovelybooks.de
  // pub" +       "lishersweekly.com everything2.com bookbrowse.com fourhourworkweek.com literatura.us viruscomix.com
  // syndetics.com kunstle" +       "r.com releituras.com mamam.ua lenguayliteratura.org bookpage.com mrdowling.com
  // booklistonline.com crookedtimber.org kids" +       "reads.com econews.gr thebookinsider.com letralia.com
  // literaturkritik.de newpages.com readinggroupguides.com fantasy-writ" +       "ers.org poems-and-quotes.com
  // altinoluk.com lambdaliterary.org megcabot.com thrillercafe.it michaelconnelly.com spaghetti" +       "bookclub.org
  // bernardcornwell.net towerofthehand.com isbndb.com bookblog.ro inlibroveritas.net poetrysociety.org alaindeb" +
  // "otton.com winningwriters.com zkungfu.com dunenovels.com languagehat.com erroluys.com " }
]
