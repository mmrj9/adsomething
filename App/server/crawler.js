import request from 'request';
import cheerio from 'cheerio';
import Future from 'fibers/future'

Meteor.startup(function() {
  if (Meteor.isServer) {
    Meteor.methods({
      pingURL: function(url) {
        var f = new Future();
        if (!url.includes('http'))
          url = `http://${url}`;
        request(url, function(error, response, html) {
          if (!error && response.statusCode == 200) {
            return f.return (true)
          } else
            return f.return (false);
          }
        );
        return f.wait()
      },
      crawlURL: function(url) {
        var f = new Future();
        if (!url.includes('http'))
          url = `http://${url}`;
        request(url, function(error, response, html) {
          if (!error && response.statusCode == 200) {
            var $ = cheerio.load(html);
            var meta = $('meta');
            var title = $("title").text();
            var keys = Object.keys(meta);
            let text = [];
        /**    $('h1,h2,h3,h4,h5,p,span').each(function(index, element) {
              if (element.children[0] && element.children[0].data) {
                text.push(element.children[0].data.trim());
              }
            }); */
            let siteData = {
              name: title || '',
              description: '',
              keywords: [],
              text: text || ''
            }
            keys.forEach(function(key) {
              if (meta[key].attribs && meta[key].attribs.name) {
                if (meta[key].attribs.name == 'description')
                  siteData.description = meta[key].attribs.content;
                else if (meta[key].attribs.name == 'keywords')
                  siteData.keywords = meta[key].attribs.content.split(",");
                }
              });
            return f.return (siteData)
          } else
            f.throw(error);
          }
        );
        return f.wait()
      }
    });
  }
});
