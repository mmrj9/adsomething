//Package Imports
import React from 'react';
import {Step, Stepper, StepLabel} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import TimePicker from 'material-ui/TimePicker';
import DatePicker from 'material-ui/DatePicker';
import Snackbar from 'material-ui/Snackbar';
import Toggle from 'material-ui/Toggle';
import Chip from 'material-ui/Chip';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import AutoComplete from 'material-ui/AutoComplete';
import Checkbox from 'material-ui/Checkbox';
import Slider from 'material-ui/Slider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import CountrySelect from "react-country-select";
import {_} from 'meteor/underscore';
//Local Imports
import CircularLoading from '../common/CircularLoading';

injectTapEventPlugin();

//Categories list
let categories;
let ageRanges;

export default class WebContentForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      //Stepper Data
      finished: false,
      stepIndex: 0,
      //Snackbar Data
      snackbarOpen: false,
      snackbarText: '',
      //Campaign Fields
      name: '',
      url: '',
      description: '',
      categories: [],
      keywords: [],
      countries: [],
      containsAdult: false,
      ageRange: '',
      //Checks
      urlOk: false,
      urlCheckInProgress: false
    };
    this.handleNext = this.handleNext.bind(this);
    this.handlePrev = this.handlePrev.bind(this);
  }

  componentWillMount() {
    Meteor.call('getStaticList', 'categories', function(err, res) {
      categories = res;
    })
    Meteor.call('getStaticList', 'ageRanges', function(err, res) {
      ageRanges = res;
    })
  }

  componentDidUpdate() {}

  // Stepper - Handle Next Step
  handleNext() {
    const {stepIndex, finished} = this.state;
    //If not last step, go to next step
    if (stepIndex < 2) {
      this.setState({
        stepIndex: stepIndex + 1
      }
      //If last step - add web content
      )
    } else {
      //Get only web content fields from state
      const {
        name,
        url,
        description,
        categories,
        keywords,
        countries,
        containsAdult,
        ageRange
      } = this.state;
      //Create web content object
      let webcontent = {
        userId: Meteor.userId(),
        name,
        url,
        description,
        categories,
        keywords,
        countries,
        containsAdult,
        ageRange
      }
      //Call to Create Campaing
      Meteor.call("createWebContent", webcontent, function(error, result) {
        if (error) {
          console.log("error", error);
        }
        if (result) {
          this.setState({finished: true, snackbarText: `Web Content ${campaing.name} successfully created`, snackbarOpen: true});
        }
      }.bind(this));
    }
  };

  // Stepper - Previous Step
  handlePrev() {
    const {stepIndex} = this.state;
    if (stepIndex > 0) {
      this.setState({
        stepIndex: stepIndex - 1
      });
    }
  };

  //Returns categoryitems for the select list
  categoryItems(values) {
    return categories.map((category, index) => (<MenuItem
      key={index}
      insetChildren={true}
      checked={values && values.includes(category)}
      value={category}
      primaryText={category}/>));
  }

  //Returns items for the select list
  ageRangeItems(values) {
    return ageRanges.map((range, index) => (<MenuItem key={index} value={range} primaryText={range}/>));
  }

  //Returns the currently selected keywords
  getKeywords() {
    return this.state.keywords.map((keyword, index) => (
      <Chip key={index} onRequestDelete={(event, index) => this.handleRequestDelete(keyword)}>
        {keyword}
      </Chip>

    ))
  }
  //Deletes chip
  handleRequestDelete(keyword) {
    let keywords = _.without(this.state.keywords, keyword);
    this.setState({keywords})
  }

  addKeyword(value) {
    this.refs.AutoComplete.setState({searchText: ''});
    if (_.contains(this.state.keywords, value))
      alert(`${value} is already a keyword`)
    else {
      this.state.keywords.push(value);
      this.setState({keywords: this.state.keywords});
    }
  }

  /**
* TODO - OnBlur only firing once
 */
  onURLBlur(event) {
    let url = event.target.value;
    this.setState({urlCheckInProgress: true});
    Meteor.call('pingURL', url, function(err, res) {
      if (res == false) {
        this.setState({urlCheckInProgress: false, urlOk: false, snackbarText: `We could make an analysis of your web content. Please check if the url is correct.`, snackbarOpen: true});
      } else {
        Meteor.call('crawlURL', url, function(err, res) {
          let name = (res.name != ''
            ? res.name
            : this.state.name);
          let description = (res.description != ''
            ? res.description
            : this.state.description);
          let keywords = (res.keywords.length > 0
            ? res.keywords
            : this.state.keywords);
          Meteor.call('predictCategory', res, function(err, res) {
            let categories = this.state.categories;
            categories.push(res);
            this.setState({name, description, keywords, categories, urlCheckInProgress: false});
          }.bind(this));
        }.bind(this));
        this.setState({urlOk: true, snackbarText: `Web content analysis successfully completed`, snackbarOpen: true});
      }
    }.bind(this));
  }

  //Gets the content for each of the 3 steps
  getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <div>
            <TextField
              floatingLabelText="Url"
              value={this.state.url}
              onChange={(event) => this.setState({url: event.target.value})}
              onBlur={(event) => this.onURLBlur(event)}
              underlineStyle={this.state.urlOk
              ? {
                borderColor: '#4CAF50'
              }
              : {}}/>
            <br/>
            <TextField
              floatingLabelText="Name"
              value={this.state.name}
              onChange={(event) => this.setState({name: event.target.value})}/>
            <br/>
            <TextField
              hintText="Message Field"
              floatingLabelText="Description"
              value={this.state.description}
              multiLine={true}
              rows={2}
              onChange={(event) => this.setState({description: event.target.value})}/>
          </div>
        );
      case 1:
        return (
          <div>
            <SelectField
              multiple={true}
              hintText="Select categories (3 Max)"
              value={this.state.categories}
              onChange={(event, index, values) => {
              if (values.length <= 3)
                this.setState({categories: values})
            }}>
              {this.categoryItems(this.state.categories)}
            </SelectField>
            <br/>
            <AutoComplete
              ref='AutoComplete'
              floatingLabelText="Type some keywords"
              filter={AutoComplete.fuzzyFilter}
              dataSource={this.state.keywords}
              maxSearchResults={5}
              onNewRequest={(value, index) => this.addKeyword(value)}/>
            <div style={{
              display: 'flex',
              flexWrap: 'wrap'
            }}>
              {this.getKeywords()}
            </div>
            <br/>
            <Checkbox
              label="Contains Adult Content"
              checked={this.state.displayAdult}
              onCheck={(event, displayAdult) => this.setState({displayAdult})}/>
          </div>
        );
      case 2:
        return (
          <div>
            <CountrySelect multi={true} flagImagePath="/img/flags/" onSelect={(countries) => this.setState({countries})}/>
            <br/>
            <SelectField
              floatingLabelText="Specify Age Range"
              value={this.state.ageRange}
              onChange={(event, index, ageRange) => this.setState({ageRange})}>
              {this.ageRangeItems()}
            </SelectField>
          </div>
        );
      default:
        return 'Ops you shouldn\'t be here ';
    }
  }

  render() {
    const contentStyle = {
      margin: '0 16px'
    };

    return (
      <div style={{
        width: '100%',
        maxWidth: 700,
        margin: 'auto'
      }}>
        <h1>Web Site Form</h1>
        <Stepper activeStep={this.state.stepIndex}>
          <Step>
            <StepLabel>Site Data</StepLabel>
          </Step>
          <Step>
            <StepLabel>Content</StepLabel>
          </Step>
          <Step>
            <StepLabel>Traffic</StepLabel>
          </Step>
        </Stepper>
        <div style={contentStyle}>
          {this.state.finished
            ? (
              <p>
                <a
                  href="#"
                  onClick={(event) => {
                  event.preventDefault();
                  this.setState({stepIndex: 0, finished: false});
                }}>
                  Click here
                </a>
                to reset the example.
              </p>
            )
            : (
              <div>
                {this.getStepContent(this.state.stepIndex)}
                <div style={{
                  marginTop: 12
                }}>
                  <FlatButton
                    label="Back"
                    disabled={this.state.stepIndex === 0}
                    onTouchTap={this.handlePrev}
                    style={{
                    marginRight: 12
                  }}/>
                  <RaisedButton
                    label={this.state.stepIndex === 2
                    ? 'Finish'
                    : 'Next'}
                    primary={true}
                    onTouchTap={this.handleNext}/>
                </div>
              </div>
            )
}
        </div>
        <Snackbar
          open={this.state.snackbarOpen}
          message={this.state.snackbarText}
          autoHideDuration={4000}
          onRequestClose={() => this.setState({snackbarText: '', snackbarOpen: false})}/> {this.state.urlCheckInProgress
          ? <CircularLoading text={'Analysing your web content'}/>
          : <div></div>}
      </div>
    );
  }
}
const styles = {
  dropzoneStyle: {
    "width": "100%",
    "borderWidth": "2px",
    "borderColor": "rgb(102, 102, 102)",
    "borderStyle": "dotted",
    "borderRadius": "5px",
    "minHeight": "100px"
  }
}
